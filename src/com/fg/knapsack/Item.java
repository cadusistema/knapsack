package com.fg.knapsack;

public class Item {

	public String name;
	public int value;
	public int weight;
	public int volume;
	
	public Item(String name, int value, int weight, int volume) {
		this.name = name;
		this.value = value;
		this.weight = weight;
		this.volume = volume;
	}

	@Override
	public String toString() {
		return "Item [name=" + name + ", value=" + value + ", weight=" + weight + ", volume=" + volume + "]";
	}
	
	

}
