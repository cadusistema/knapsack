package com.fg.knapsack;

import java.util.ArrayList;
import java.util.List;

public class Solution {
	
	// list of items to put in the bag to have the maximal value
		public List<Item> items;
		// maximal value possible
		public int totalValue;
		public int totalVolume;
		
		
		public Solution(List<Item> items, int value) {
			this.items = items;
			this.totalValue = value;
		}
		
		public Solution(Solution solution, Item item) {
			if(this.items ==null)
				this.items = new ArrayList<Item>();
			if(solution != null)
				this.items.addAll(solution.items);
			
			this.items.add(item);
			this.totalValue = 0;
			this.totalVolume = 0;
			for (Item aux : items) {
				totalValue+= aux.value;
				totalVolume+= aux.volume;
			}
			
			
		}

		public void display() {
			if (items != null  &&  !items.isEmpty()){
				System.out.println("\nKnapsack solution");
				System.out.println("Value = " + totalValue);
				System.out.println("Items to pick :");
				
				for (Item item : items) {
					System.out.println("- " + item);
				}
			}
		}

		@Override
		public String toString() {
			return "totalItems= "+items.size()+" totalValue=" + totalValue +  " items=" + items + ", totalVolume=" + totalVolume ;
		}
		
		
		
		


}
