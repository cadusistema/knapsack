package com.fg.knapsack;

import java.util.ArrayList;
import java.util.List;

public class Knapsack {

	// items of our problem
	private Item[] items;
	// capacity of the bag
	private int maxWeight;
	private int totalVolume;


	public Knapsack(Item[] items, int maxWeight, int totalVolume) {
		this.items = items;
		this.maxWeight = maxWeight;
		this.totalVolume = totalVolume;
	}


	public void display() {
		if (items != null && items.length > 0) {
			System.out.println("Knapsack problem");
			System.out.println("Max Weight : " + maxWeight);
			System.out.println("Max Volume : " + totalVolume);
			System.out.println("Items :");

			for (Item item : items) {
				System.out.println("- " + item);
			}
		}
	}


	// used in the solve3
	private Solution max3(Solution s1, Solution s2) {
		if (s1 == null)
			return s2;

		if (s1.totalValue > s2.totalValue)
			return s1;
		else {
			if (s2.totalVolume > this.totalVolume)
				return s1;
			else
				return s2;
		}
	}


	// solution that uses infinite items in store
	public Solution solve3() {
		int quantityItems = items.length;
		Solution dp[] = new Solution[maxWeight + 1];
		for (int i = 0; i <= maxWeight; i++) {
			for (int j = 0; j < quantityItems; j++) {
				if (items[j].weight <= i) {
					dp[i] = max3(dp[i], new Solution(dp[i - items[j].weight], items[j]));
				}
			}
		}
		return dp[maxWeight];
	}


	// used in the solve 2
	private static Solution max(Solution s1, Solution s2) {
		if (s1 == null)
			return s2;
		return (s1.totalValue > s2.totalValue) ? s1 : s2;

	}


	// solution that uses infinite items in store
	public Solution solve2() {
		int quantityItems = items.length;
		Solution dp[] = new Solution[maxWeight + 1];
		for (int i = 0; i <= maxWeight; i++) {
			for (int j = 0; j < quantityItems; j++) {
				if (items[j].weight <= i) {
					dp[i] = max(dp[i], new Solution(dp[i - items[j].weight], items[j]));

				}
			}
		}
		return dp[maxWeight];
	}


	// solution that uses only one item a time
	public Solution solve() {
		int NB_ITEMS = items.length;
		// we use a matrix to store the max value at each n-th item
		int[][] matrix = new int[NB_ITEMS + 1][maxWeight + 1];

		// first line is initialized to 0
		for (int i = 0; i <= maxWeight; i++)
			matrix[0][i] = 0;

		// we iterate on items
		for (int i = 1; i <= NB_ITEMS; i++) {
			// we iterate on each capacity
			for (int j = 0; j <= maxWeight; j++) {
				if (items[i - 1].weight > j)
					matrix[i][j] = matrix[i - 1][j];
				else
					// we maximize value at this rank in the matrix
					matrix[i][j] = Math.max(matrix[i - 1][j], matrix[i - 1][j - items[i - 1].weight] + items[i - 1].value);
			}
		}

		int res = matrix[NB_ITEMS][maxWeight];
		int w = maxWeight;
		List<Item> itemsSolution = new ArrayList<>();

		for (int i = NB_ITEMS; i > 0 && res > 0; i--) {
			if (res != matrix[i - 1][w]) {
				itemsSolution.add(items[i - 1]);
				// we remove items value and weight
				res -= items[i - 1].value;
				w -= items[i - 1].weight;
			}
		}

		return new Solution(itemsSolution, matrix[NB_ITEMS][maxWeight]);
	}


	public static void main(String[] args) {
		// we take the same instance of the problem displayed in the image
		Item[] items = {new Item("Ore",80 ,1 ,1 ),
	    		new Item("Grill",600 ,7 ,7 ),
	    		new Item("Light Bulb",360 ,4 ,4 ),
	    		new Item("Gear",100 ,1 ,2 ),
	    		new Item("Liquid",100 ,1 ,2 ),
	    		new Item("Wire",100 ,1 ,2 ),
	    		new Item("Circuit",300 ,3 ,3 ),
	    		new Item("Antenna",540 ,5 ,5 ),
	    		new Item("Clock",540 ,5 ,5 ),
	    		new Item("Cooler Plate",360 ,3 ,3 ),
	    		new Item("Engine",360 ,3 ,3 ),
	    		new Item("Heater Plate",360 ,3 ,3 ),
	    		new Item("Washing Machine",1100 ,7 ,7 ),
	    		new Item("Processor",1320 ,8 ,8 ),
	    		new Item("Air Conditioner",900 ,5 ,5 ),
	    		new Item("Toaster",900 ,5 ,5 ),
	    		new Item("Solar Panel",1170 ,6 ,6 ),
	    		new Item("Battery",1050 ,5 ,5 ),
	    		new Item("Power Supply",1920 ,9 ,9 ),
	    		new Item("Drill",1500 ,7 ,7 ),
	    		new Item("Advanced Engine",70000 ,300 ,300 ),
	    		new Item("Speakers",3300 ,14 ,14 ),
	    		new Item("Plate",250 ,1 ,2 ),
	    		new Item("HeadPhones",1300 ,5 ,12 )
//	    		new Item("Electric Engine",900000 ,3200 ,3200 ),
//	    		new Item("Microwave",8070 ,25 ,25 ),
//	    		new Item("Jackhammer",6920 ,20 ,20 ),
//	    		new Item("Server Rack",10600 ,30 ,30 ),
//	    		new Item("Electric Board",27000 ,72 ,72 ),
//	    		new Item("Fridge",7400 ,18 ,18 ),
//	    		new Item("Railway",8400 ,20 ,20 ),
//	    		new Item("Radio",5670 ,13 ,13 ),
//	    		new Item("TV",7100 ,16 ,16 ),
//	    		new Item("Tablet",7600 ,17 ,17 ),
//	    		new Item("Computer",11000 ,23 ,23 ),
//	    		new Item("SmartPhone",7300 ,15 ,15 ),
//	    		new Item("Super Computer",550000 ,1105 ,1105 ),
//	    		new Item("Water Heater",12900 ,25 ,25 ),
//	    		new Item("SmartWatch",10170 ,19 ,19 ),
//	    		new Item("Generator",11820 ,22 ,22 ),
//	    		new Item("Oven",27300 ,50 ,50 ),
//	    		new Item("Lazer",31800 ,58 ,58 ),
//	    		new Item("Drone",17220 ,30 ,30 ),
//	    		new Item("AI Processor",2500000 ,4080 ,4080 ),
//	    		new Item("AI Robot Body",2800000 ,4280 ,4280 ),
//	    		new Item("Electric Generator",470000 ,680 ,680 ),
//	    		new Item("AI Robot Head",5000000 ,4280 ,4280 ),
//	    		new Item("AI Robot",15000000 ,8560 ,8560 )
	                    };

		Knapsack knapsack = new Knapsack(items, 26, 1000);
		knapsack.display();
		Solution solution = knapsack.solve3();
		solution.display();
	}

}
